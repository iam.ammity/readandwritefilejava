import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.LongStream;

public class ReadWriteFile {
    public static void main(String[] args) {
        String location = ReadWriteFile.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        //read file
        String data = readFile(location);

        //prepare content
        String[] arrOfStr = data.split("\\|", 2);
        long numStart = Long.parseLong(arrOfStr[0]);
        long numEnd = Long.parseLong(arrOfStr[1]);

        //write file
        writeFile(location,genRunning(numStart,numEnd));
    }
    private static String readFile(String path) {
        String response = "";
        try {
            File myObj = new File(path+"input.txt");
            Scanner myReader = new Scanner(myObj);
            response = myReader.nextLine();
            myReader.close();

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return response;
    }
    private static void writeFile(String path,String content) {
        try {
            FileWriter myWriter = new FileWriter(path + "output.txt");
            myWriter.write(content);
            myWriter.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
    private static String genRunning(Long start,Long end) {
        long[] range = LongStream.rangeClosed(start, end).parallel().toArray();
        return Arrays.toString(range).replace(",", "\n").replace("[", "").replace("]", "").replace(" ", "");
    }
}
